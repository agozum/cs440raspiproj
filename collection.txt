          Cell 01 - Address: 94:B4:0F:98:70:90
                    Frequency:5.2 GHz (Channel 40)
                    Quality=56/70  Signal level=-54 dBm  
                    Encryption key:on
                    ESSID:"eduroam"
                    IE: IEEE 802.11i/WPA2 Version 1
                        Authentication Suites (1) : 802.1x
          Cell 02 - Address: 94:B4:0F:98:72:C0
                    Frequency:2.412 GHz (Channel 1)
                    Quality=57/70  Signal level=-53 dBm  
                    Encryption key:on
                    ESSID:"eduroam"
                    IE: IEEE 802.11i/WPA2 Version 1
                        Authentication Suites (1) : 802.1x
          Cell 03 - Address: 94:B4:0F:98:87:80
                    Frequency:2.412 GHz (Channel 1)
                    Quality=54/70  Signal level=-56 dBm  
                    Encryption key:on
                    ESSID:"eduroam"
                    IE: IEEE 802.11i/WPA2 Version 1
                        Authentication Suites (1) : 802.1x