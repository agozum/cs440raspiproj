#!/bin/bash
###############################################################
# Name: collection.sh
# Description: Collects data of locally available WiFi networks
# Usage: collection.sh <noargs>
# Last updated: 2019-4-29
###############################################################

#Ensures WiFi_Data directory created in home directory
if [ ! -d ~/WiFi_Data ]; then
	mkdir ~/WiFi_Data
fi


_dir=$(date +'%F')                 # Creates date based directory
_time=$(date +'%FT%H%M%S')         # ISO 8601 compliant date & time
_file="WiFi_Networks_$_time.txt"   # Filename of each WiFi network scan


# Date based directory check
if [ ! -d ~/WiFi_Data/$_dir ]; then
	mkdir ~/WiFi_Data/$_dir
fi

# Redirect WiFi information to given filename within date based directory
sudo iwlist wlan0 scan | grep -iE 'Address|IEEE|Freq|Qual|Encryp|Auth|SSID' >> ~/WiFi_Data/$_dir/$_file
